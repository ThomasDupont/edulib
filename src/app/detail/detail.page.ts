import { OnInit } from '@angular/core';
import { Component } from '@angular/core';
import { ActivatedRoute, NavigationEnd, Router } from '@angular/router';
import { EdulibApiService } from '../services/edulib-api.service.ngtypecheck';
import { GetRepoResponseDataType } from '../types/github.types';
import { ROOT } from '../app-routing.module';
import { RoutesMapping } from '../types/routes.types';

@Component({
  selector: 'app-tab3',
  templateUrl: 'detail.page.html',
  styleUrls: ['detail.page.scss']
})
export class DetailPage implements OnInit {
  public repository: GetRepoResponseDataType;

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private edulibBack: EdulibApiService,
  ) {}

  public ngOnInit(): void {
    this.router.events.subscribe((event) => {
      if (event instanceof NavigationEnd && event.urlAfterRedirects.split('?').shift() === `/${ROOT}/${RoutesMapping.Detail}`) {
        const { owner, repo } = this.route.snapshot.queryParams;
        // prevent undefined navigation (should be protected with a Guard)
        this.edulibBack.getRepository(owner ?? 'octokit', repo ?? 'octokit.rb').subscribe((repository: GetRepoResponseDataType) => {
          this.repository = repository;
        });
      }
    });

  }
}
