import { Component } from '@angular/core';
import { NavController } from '@ionic/angular';
import { ROOT } from '../app-routing.module';
import { RoutesMapping } from '../types/routes.types';

@Component({
  selector: 'app-tab1',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss']
})
export class HomePage {
  public org: string;

  constructor(private navCtrl: NavController) {}

  public list() {
    this.navCtrl.navigateRoot(`/${ROOT}/${RoutesMapping.List}`, {
      ... (this.org && {
        queryParams: {
          org: this.org,
        }
      })
    });
  }
}
