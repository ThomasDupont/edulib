import { Component, OnInit } from '@angular/core';
import { EdulibApiService } from '../services/edulib-api.service.ngtypecheck';
import { ActivatedRoute, NavigationEnd, Router } from '@angular/router';
import { NavController } from '@ionic/angular';
import { ListOrgReposResponseDataType } from '../types/github.types';
import { ROOT } from '../app-routing.module';
import { RoutesMapping } from '../types/routes.types';

@Component({
  selector: 'app-tab2',
  templateUrl: 'list.page.html',
  styleUrls: ['list.page.scss']
})
export class ListPage implements OnInit {

  public owner: string;
  public repos: ListOrgReposResponseDataType;
  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private edulibBack: EdulibApiService,
    private navCtrl: NavController
  ) {}

  public ngOnInit(): void {
    this.router.events.subscribe((event) => {
      // detect navigation change
      if (event instanceof NavigationEnd && event.urlAfterRedirects.split('?').shift() === `/${ROOT}/${RoutesMapping.List}`) {
        // prevent undefined navigation (should be protected with a Guard)
        this.owner = this.route.snapshot.queryParams.org ?? 'octokit';
        this.edulibBack.listOrgRepositories(this.owner, 1).subscribe((repos: ListOrgReposResponseDataType) => {
          this.repos = repos;
        });
      }
    });
  }

  public displayDetail(repo: string): void {
    this.navCtrl.navigateRoot(`/${ROOT}/${RoutesMapping.Detail}`, {
      queryParams: {
        owner: this.owner,
        repo
      }
    });
  }
}
