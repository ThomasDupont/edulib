import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { throwError } from 'rxjs';
import { catchError, retry } from 'rxjs/operators';
import { environment } from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class EdulibApiService {

  constructor(private http: HttpClient) { }

  public listOrgRepositories(org: string, page: number) {
    return this.http.get(`${environment.edulib_api_host}/github/orgs/${org}/repos?page=${page}`).
    pipe(
      retry(3),
      catchError(this.handleError)
    );
  }

  public getRepository(owner: string, repo: string) {
    return this.http.get(`${environment.edulib_api_host}/github/repos/${owner}/${repo}`).
    pipe(
      retry(3),
      catchError(this.handleError)
    );
  }

  private handleError(error: HttpErrorResponse) {
    if (error.error instanceof ErrorEvent) {
      console.error('An error occurred:', error.error.message);
    } else {
      console.error(`Backend returned code ${error.status}, body was: ${error.error}`);
    }
    return throwError('Something bad happened; please try again later.');
  }

}
