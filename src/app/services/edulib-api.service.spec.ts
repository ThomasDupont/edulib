import { TestBed } from '@angular/core/testing';

import { EdulibApiService } from './edulib-api.service.ngtypecheck';

describe('EdulibApiService', () => {
  let service: EdulibApiService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(EdulibApiService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
