import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ROOT } from '../app-routing.module';
import { RoutesMapping } from '../types/routes.types';
import { TabsPage } from './tabs.page';

const routes: Routes = [
  {
    path: ROOT,
    component: TabsPage,
    children: [
      {
        path: RoutesMapping.Home,
        loadChildren: () => import('../home/home.module').then(m => m.HomePageModule)
      },
      {
        path: RoutesMapping.List,
        loadChildren: () => import('../list/list.module').then(m => m.ListPageModule)
      },
      {
        path: RoutesMapping.Detail,
        loadChildren: () => import('../detail/detail.module').then(m => m.DetailPageModule)
      },
      {
        path: '',
        redirectTo: `/${ROOT}/${RoutesMapping.Home}`,
        pathMatch: 'full'
      }
    ]
  },
  {
    path: '',
    redirectTo: `/${ROOT}/${RoutesMapping.Home}`,
    pathMatch: 'full'
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
})
export class TabsPageRoutingModule {}
