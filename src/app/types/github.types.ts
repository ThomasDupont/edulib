import { Octokit } from '@octokit/rest';
import {
  GetResponseDataTypeFromEndpointMethod,
  OctokitResponse,
} from '@octokit/types';

const octokit = new Octokit();

export type ListOrgReposResponseDataType = OctokitResponse<GetResponseDataTypeFromEndpointMethod<
    typeof octokit.repos.listForOrg
>>;

export type GetRepoResponseDataType = OctokitResponse<GetResponseDataTypeFromEndpointMethod<
    typeof octokit.repos.get
>>;
