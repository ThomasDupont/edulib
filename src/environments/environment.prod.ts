export const environment = {
  production: true,
  edulib_api_host: 'https://edulib-back.herokuapp.com'
};
